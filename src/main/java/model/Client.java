package model;


public class Client {
    /*
    The client class models the elements of the queues, each client having an id, arrival and service time.
     */
    private int id;
    private int arrivalTime;
    private int serviceTime;
    public Client(int serviceTime) {
        this.serviceTime = serviceTime;
    }
    public int getArrivalTime() { return arrivalTime; }
    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    public int getServiceTime() { return serviceTime; }

    @Override
    public String toString() {
        /*
         Useful in the Simulation text
         */
        return "Client" + id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
