package model;

public class Log {
    /*
    Log class is a separate class, not having anything to do with pre-existing logging classes in Java.
    It contains a StringBuilder object that can have log messages appended to it.
    To get the contents of the log, the getLog() method was used.
     */
    private StringBuilder stringBuilder;
    public Log() {
        stringBuilder = new StringBuilder();
    }
    public void add(String text) {
        stringBuilder.append(text).append('\n');
    }
    public String getLog() {
        return stringBuilder.toString();
    }
}
