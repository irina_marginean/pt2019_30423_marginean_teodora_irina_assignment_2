import controllers.MainController;
import views.View;

public class Application {
    /*
    The class that contains the main method.
    It initializes the view and the controller.
     */
    public static void main(String args[]) {
        View view = new View();
        view.setVisible(true);
        MainController controller = new MainController(view);
        controller.start();
    }
}
