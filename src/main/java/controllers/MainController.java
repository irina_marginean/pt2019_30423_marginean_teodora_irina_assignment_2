package controllers;

import model.Log;
import views.View;
import logic.*;

public class MainController {
    /*
    The class that binds the logic tot the view.
    It initializes the ActionListener for the Start Simulation button.
    It starts the Time thread and the QueueService thread.
     */
    private View view;
    private Log log;
    public MainController(View view) {
        this.view = view;
        log = new Log();
    }
    public void start() {
        initializeButtonListeners();
    }
    private void initializeButtonListeners() {
        view.addStartButtonListener(e -> {
            try {
                int minGenerationTime = getInt(view.getMinimumGenerationTime());
                int maxGenerationTime = getInt(view.getMaximumGenerationTime());
                int minServiceTime = getInt(view.getMinimumServiceTime());
                int maxServiceTime = getInt(view.getMaximumServiceTime());
                int numberOfQueues = getInt(view.getNumberOfQueues());
                int simulationTime = getInt(view.getSimulationTime());
                Time time = new Time(simulationTime, log, view);
                Thread service = new Thread(new QueueService(numberOfQueues, minGenerationTime, maxGenerationTime,
                        simulationTime, minServiceTime, maxServiceTime, time.getCurrentTime(), view, log));
                time.start();
                service.start();
            }
            catch (NumberFormatException g) {
                view.showWarningMessage("Try again!");
            }
        });
    }
    private int getInt(String text) throws NumberFormatException{
        try {
            int value = Integer.parseInt(text);
            return value;
        }
        catch (NumberFormatException e) {
            view.showWarningMessage("You did not input a number!");
            throw new NumberFormatException();
        }
    }

}
