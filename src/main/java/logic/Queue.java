package logic;

import model.Client;
import model.Log;
import views.View;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue extends Thread {
    /*
    The Queue class models the Consumer.
     */
    private BlockingQueue<Client> clients;
    private int waitingTime = 0;
    private AtomicInteger currentTime;
    private Log log;
    private View view;
    private QueueService service;
    private int maximumWaitingTime = 0;
    private int peakTime = 0;

    public Queue(int maxCapacity, Log log, View view, QueueService service, AtomicInteger currentTime) {
        clients = new LinkedBlockingQueue<>(maxCapacity);
        this.view = view;
        this.service = service;
        this.currentTime = currentTime;
        this.log = log;
    }
    synchronized public void push(Client client) {
        try {
            clients.put(client);
            log.add("Current waiting time: " + waitingTime);
            // information relevant for the statistics
            if (waitingTime >= maximumWaitingTime) {
                maximumWaitingTime = waitingTime;
                peakTime = currentTime.get();
            }
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    @Override
    public void run() {
                while (clients.isEmpty()) {
                    synchronized (clients) {
                        try {
                            clients.wait(1000);
                        } catch (InterruptedException e) {
                        }
                    }
                    while (!clients.isEmpty()) {
                        try {
                            // get the service time of the client at the top of the list
                            Client currentClient = clients.peek();
                            int serviceTime = currentClient.getServiceTime();
                            // the client at the top of the list will be processed sequentially,
                            // in order to get an accurate waiting time in the QueueService
                            for (int i = 1; i <= serviceTime; i++) {
                                Thread.sleep( 1000);
                                waitingTime--;
                            }
                            log.add("Removed " + currentClient.toString() + ", current waiting time: " + waitingTime);
                            view.setLogText(log.getLog());
                            // client is popped
                            clients.take();
                            view.setSimulationText(service.toString());
                            try { clients.notify(); }
                            catch (IllegalMonitorStateException e) { }
                        } catch (NullPointerException e) { Thread.currentThread().interrupt(); }
                          catch (InterruptedException e) { Thread.currentThread().interrupt(); }
                    }
                }
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void incrementWaitingTime(int waitingTime) {
        this.waitingTime += waitingTime;
    }

    @Override
    public String toString() {
        return  clients + " ";
    }

    public int getMaximumWaitingTime() {
        return maximumWaitingTime;
    }

    public int getPeakTime() {
        return peakTime;
    }
}
