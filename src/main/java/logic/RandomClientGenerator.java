package logic;

import model.Client;

import java.util.Random;
import java.util.Vector;

public class RandomClientGenerator {
    /*
    The RandomClientGenerator class is the class that generate a list of random clients, which are ultimately
     contained in a Vector of Clients.
     */
    Vector<Client> clients;
    private final int minServiceTime;
    private final int maxServiceTime;

    public RandomClientGenerator(int capacity, int minServiceTime, int maxServiceTime) {
        clients = new Vector<>(capacity);
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
    }
    public Vector<Client> generateClients(int numberOfClients) {
        for (int i = 1; i <= numberOfClients; i++) {
            Client client = generateClient();
            client.setId(i);
            clients.add(client);
        }
        return clients;
    }
    private Client generateClient() {
        int randomServiceTime = new Random().nextInt(maxServiceTime - minServiceTime + 1) + minServiceTime;
        Client currentClient = new Client(randomServiceTime);
        return  currentClient;
    }
}
