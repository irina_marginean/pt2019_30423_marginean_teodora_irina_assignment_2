package logic;

import model.Log;
import views.View;

import java.util.concurrent.atomic.AtomicInteger;

public class Time extends Thread {
    /*
    The time class is a separate thread that contains an AtomicInteger object that gets incremented by 1 every second
    in the run method.
     */
    private AtomicInteger currentTime;
    private int simulationTime;
    private Log log;
    private View view;
    public Time(int simulationTime, Log log, View view) {
        this.simulationTime = simulationTime;
        this.log = log;
        this.view = view;
        currentTime = new AtomicInteger(0);
    }

    @Override
    public void run() {
        while (currentTime.get() < simulationTime) {
            try {
                Thread.sleep(1000);
                currentTime.getAndIncrement();
                log.add("time: " + currentTime + ", simulation time: " + simulationTime);
                view.setLogText(log.getLog());
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public AtomicInteger getCurrentTime() {
        return currentTime;
    }
}
