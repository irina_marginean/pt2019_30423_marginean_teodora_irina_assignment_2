package logic;

import model.Client;
import model.Log;
import views.View;

import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class QueueService implements  Runnable {
    /*
    The Producer thread.
     */
    private int maxNumberOfClients;
    private int numberOfQueues;
    private int minGenerationTime;
    private int maxGenerationTime;
    private int simulationTime;
    private int minServiceTime;
    private int maxServiceTime;
    private List<Queue> queues;
    private AtomicInteger currentTime;
    private final Vector<Client> clientsList;
    private View view;
    private Log log;
    private int totalNumberOfClients = 0;
    private volatile boolean exit = false;

    public QueueService(int numberOfQueues, int minGenerationTime, int maxGenerationTime,
                        int simulationTime, int minServiceTime, int maxServiceTime, AtomicInteger currentTime,
                        View view, Log log) {
        // Initialization of private fields.
        this.numberOfQueues = numberOfQueues;
        this.minGenerationTime = minGenerationTime;
        this.maxGenerationTime = maxGenerationTime;
        this.minServiceTime = minServiceTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.simulationTime = simulationTime;
        this.currentTime = currentTime;
        this.view = view;
        this.log = log;
        // I made the choice that the maximum number of generated clients will be equal to the simulation time,
        // so as to never run out of clients.
        maxNumberOfClients = simulationTime;
        // Client list generation
        RandomClientGenerator clientGenerator = new RandomClientGenerator(maxNumberOfClients, minServiceTime, maxServiceTime);
        clientsList = clientGenerator.generateClients(maxNumberOfClients);
        // Creation and starting of the Consumer threads
        synchronized (clientsList) {
            queues = Collections.synchronizedList(new ArrayList<>(numberOfQueues));
            for (int i = 0; i < numberOfQueues; i++) {
                queues.add(new Queue(maxNumberOfClients/numberOfQueues, log, view, this, currentTime));
                queues.get(i).start();
            }
        }
    }

    private Queue selectQueueToInsertClient() {
        // Selects the queue with the minumum waiting time.
        Queue minQueue = queues.get(0);
        for (Queue queue : queues) {
            if (minQueue.getWaitingTime() > queue.getWaitingTime()) {
                minQueue = queue;
            }
        }
        return minQueue;
    }

    private int getRandomGenerationTime() {
        int random = new Random().nextInt(maxGenerationTime - minGenerationTime + 1) + minGenerationTime;
        return random;
    }

    private double getAverageWaitingTime() {
        // Used for statistics.
        double average = 0;
        double totalTime = 0;
        for (Queue queue : queues) {
            totalTime += queue.getWaitingTime();
        }
        average = totalTime * 1.0 / numberOfQueues;
        return average;
    }
    private int getPeakTime() {
        // Used for statistics.
        int peakTime = 0;
        int globalMaxWaitingTime = 0;
        for (Queue queue : queues) {
                if (queue.getMaximumWaitingTime() > globalMaxWaitingTime) {
                    globalMaxWaitingTime = queue.getMaximumWaitingTime();
                    peakTime = queue.getPeakTime();
            }
        }
        return peakTime;
    }
    private int getMaxWaitingTime() {
        int globalMaxWaitingTime = 0;
        for (Queue queue : queues) {
            if (queue.getMaximumWaitingTime() > globalMaxWaitingTime) {
                globalMaxWaitingTime = queue.getMaximumWaitingTime();

            }
        }
        return globalMaxWaitingTime;
    }
    private void setView() {
        // A separate method that is called in the run method.
        // I chose to create this method because there was too much code in the run method
        // It sets the view according to the current state of the queues
        view.setPeakTime(getPeakTime() + "");
        view.setAvgWaitingTime(getAverageWaitingTime() + "");
        view.setMaxWaitingTime(getMaxWaitingTime() + "");
        view.setSimulationText(toString());
        view.setLogText(log.getLog());
        view.setTotalNumberOfClients(totalNumberOfClients + "");
    }

    @Override
    public void run() {
        synchronized (clientsList) {
            log.add("SIMULATION STARTED!");
            while (!exit) {
                // the exit variable signals when the simulation timje has stopped
                try {
                    // Select the queue to insert the client and remove the client from the Vector
                    Queue queueToInsert = selectQueueToInsertClient();
                    Client clientToInsert = clientsList.remove(0);
                    // Pause for a random time before inserting the client
                    Thread.sleep(getRandomGenerationTime() * 1000);
                    // the arrival time of the client to be inserted will be the current time (from the Time class)
                    clientToInsert.setArrivalTime(currentTime.get());
                    log.add("Added " + clientToInsert.toString() + ", having service time " + clientToInsert.getServiceTime());
                    log.add("Arrived at: " + clientToInsert.getArrivalTime());
                    view.setSimulationText(toString());
                    // increase the waiting time for the selected queue
                    queueToInsert.incrementWaitingTime(clientToInsert.getServiceTime());
                    //push the client into the selected queue
                    queueToInsert.push(clientToInsert);
                    totalNumberOfClients++;
                    setView();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                if (currentTime.get() == simulationTime) {
                    log.add("SIMULATION TIME STOPPED!");
                    view.setLogText(log.getLog());
                    exit = true;
                }
            }
        }
    }

    @Override
    public String toString() {
        // Used to display the queue evolution in the view
        StringBuilder stringBuilder = new StringBuilder();
        int i = 1;
        for (Queue queue : queues) {
            stringBuilder.append("Queue").append(i).append(": ").append(queue.toString()).append('\n');
            i++;
        }
        return stringBuilder.toString();
    }
}