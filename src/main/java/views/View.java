/*
 * Created by JFormDesigner on Tue Apr 02 01:39:09 EEST 2019
 */

package views;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;


public class View extends JFrame {
    public View() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
            System.out.println("Error");
        }
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Irina Marginean
        textField1 = new JTextField();
        textField2 = new JTextField();
        textField3 = new JTextField();
        textField4 = new JTextField();
        textField5 = new JTextField();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        label4 = new JLabel();
        label5 = new JLabel();
        button1 = new JButton();
        scrollPane1 = new JScrollPane();
        textArea1 = new JTextArea();
        scrollPane2 = new JScrollPane();
        textArea2 = new JTextArea();
        label6 = new JLabel();
        label7 = new JLabel();
        textField6 = new JTextField();
        label8 = new JLabel();
        scrollPane3 = new JScrollPane();
        avgWaitingTime = new JTextPane();
        label9 = new JLabel();
        scrollPane4 = new JScrollPane();
        peakTime = new JTextPane();
        label10 = new JLabel();
        label11 = new JLabel();
        scrollPane5 = new JScrollPane();
        totalNumberOfClients = new JTextPane();
        label12 = new JLabel();
        scrollPane6 = new JScrollPane();
        maxWaitingTime = new JTextPane();
        label13 = new JLabel();

        //======== this ========
        setTitle("Queue Simulator");
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Minimum Generation Time");

        //---- label2 ----
        label2.setText("Maximum Generation Time");

        //---- label3 ----
        label3.setText("Minimum Service Time");

        //---- label4 ----
        label4.setText("Maximum Service Time");

        //---- label5 ----
        label5.setText("Simulation time");

        //---- button1 ----
        button1.setText("Start Simulation");

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(textArea1);
        }

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(textArea2);
        }

        //---- label6 ----
        label6.setText("Simulation");

        //---- label7 ----
        label7.setText("Log");

        //---- label8 ----
        label8.setText("Number of queues");

        //======== scrollPane3 ========
        {
            scrollPane3.setViewportView(avgWaitingTime);
        }

        //---- label9 ----
        label9.setText("Average Waiting Time");

        //======== scrollPane4 ========
        {
            scrollPane4.setViewportView(peakTime);
        }

        //---- label10 ----
        label10.setText("Peak Time");

        //---- label11 ----
        label11.setText("Statistics");
        label11.setFont(label11.getFont().deriveFont(label11.getFont().getSize() + 6f));
        label11.setForeground(UIManager.getColor("CheckBoxMenuItem.acceleratorForeground"));

        //======== scrollPane5 ========
        {
            scrollPane5.setViewportView(totalNumberOfClients);
        }

        //---- label12 ----
        label12.setText("Total number of clients");

        //======== scrollPane6 ========
        {
            scrollPane6.setViewportView(maxWaitingTime);
        }

        //---- label13 ----
        label13.setText("Maximum waiting time");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                        .addComponent(label1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(label3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(label2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                        .addComponent(textField3, GroupLayout.Alignment.LEADING)
                        .addComponent(textField1, GroupLayout.Alignment.LEADING)
                        .addComponent(textField2, GroupLayout.Alignment.LEADING)
                        .addComponent(label4, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(button1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(textField4)
                        .addComponent(label8, GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                        .addComponent(textField6, GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                        .addComponent(textField5)
                        .addComponent(label5, GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(label6, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(515, Short.MAX_VALUE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addComponent(scrollPane1, GroupLayout.Alignment.LEADING)
                                .addGroup(GroupLayout.Alignment.LEADING, contentPaneLayout.createSequentialGroup()
                                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 272, Short.MAX_VALUE))
                                .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
                            .addGap(45, 45, 45)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(label11, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                .addComponent(label10, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                .addComponent(scrollPane4, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                .addComponent(label13)
                                .addComponent(scrollPane6, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                .addComponent(scrollPane5)
                                .addComponent(label12, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                .addComponent(scrollPane3, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                .addComponent(label9, GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))
                            .addGap(23, 23, 23))))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label1)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(textField1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(textField2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addGap(1, 1, 1)
                    .addComponent(textField3, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(textField4, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(label8)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                    .addComponent(textField6, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(label5, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(textField5, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(button1, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                    .addGap(58, 58, 58))
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(88, 88, 88)
                    .addComponent(label11)
                    .addGap(24, 24, 24)
                    .addComponent(label9)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(scrollPane3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(label10)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(scrollPane4, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(label13)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(scrollPane6, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(label12, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(scrollPane5, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(148, Short.MAX_VALUE))
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(label6)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(221, 221, 221)
                            .addComponent(label7)))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Irina Marginean
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;
    private JButton button1;
    private JScrollPane scrollPane1;
    private JTextArea textArea1;
    private JScrollPane scrollPane2;
    private JTextArea textArea2;
    private JLabel label6;
    private JLabel label7;
    private JTextField textField6;
    private JLabel label8;
    private JScrollPane scrollPane3;
    private JTextPane avgWaitingTime;
    private JLabel label9;
    private JScrollPane scrollPane4;
    private JTextPane peakTime;
    private JLabel label10;
    private JLabel label11;
    private JScrollPane scrollPane5;
    private JTextPane totalNumberOfClients;
    private JLabel label12;
    private JScrollPane scrollPane6;
    private JTextPane maxWaitingTime;
    private JLabel label13;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }
    public String getMinimumGenerationTime() {
        if (textField1.getText().equals("")) {
            showWarningMessage("You did not input the minimum clients generation time");
            return "";
        }
        return textField1.getText();
    }
    public String getMaximumGenerationTime() {
        if (textField1.getText().equals("")) {
            showWarningMessage("You did not input the maximum clients generation time");
            return "";
        }
        return textField2.getText();
    }
    public String getMinimumServiceTime() {
        if (textField1.getText().equals("")) {
            showWarningMessage("You did not input the minimum service time");
            return "";
        }
        return textField3.getText();
    }
    public String getMaximumServiceTime() {
        if (textField1.getText().equals("")) {
            showWarningMessage("You did not input the maximum service time");
            return "";
        }
        return textField4.getText();
    }
    public String getSimulationTime() {
        if (textField1.getText().equals("")) {
            showWarningMessage("You did not input the simulationTime");
            return "";
        }
        return textField5.getText();
    }
    public String getNumberOfQueues() {
        if (textField1.getText().equals("")) {
            showWarningMessage("You did not input the number of queues");
            return "";
        }
        return textField6.getText();
    }
    public void addStartButtonListener(final ActionListener actionListener) {
        button1.addActionListener(actionListener);
    }
    public void setSimulationText(String text) {
        textArea1.setText(text);
    }
    public void setLogText(String text) {
        textArea2.setText(text);
    }
    public void setAvgWaitingTime(String text) {
        avgWaitingTime.setText(text);
    }
    public void setPeakTime(String text) {
        peakTime.setText(text);
    }
    public void setTotalNumberOfClients(String text) {
        totalNumberOfClients.setText(text);
    }
    public void setMaxWaitingTime(String text) {
        maxWaitingTime.setText(text);
    }
}
